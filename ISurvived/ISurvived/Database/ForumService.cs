﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.WindowsAzure.MobileServices;
using System.Diagnostics;
using Microsoft.WindowsAzure.MobileServices.Sync;


namespace ISurvived
{
    public class ForumService
	{

        private IMobileServiceClient client;
        IMobileServiceSyncTable<User> userTable;
        IMobileServiceSyncTable<Post> postTable;
        IMobileServiceSyncTable<Comment> commentTable;


       // new App.PostExpandHandler()
        
		/// <summary>
		/// Initializes a new instance of the <see cref="NoteTaker1.Data.NoteDatabase"/> class.
		/// </summary>
		public ForumService (IMobileServiceClient client)
		{

            // Initialize the Mobile Service client with your URL and key
           this.client = client;

            // Create an MSTable instance to allow us to work with the TodoItem table
            userTable = client.GetSyncTable<User>();
            postTable = client.GetSyncTable<Post>();
            commentTable = client.GetSyncTable<Comment>();
        }



        //done
        public async Task<User> GetUserAsync(string id)
        {
            try
            {
                await SyncAsync();
                return await userTable.LookupAsync(id);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"INVALID {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"ERROR {0}", e.Message);
            }
            return null;
        }

        //done
		public async Task<List<Post>> GetAllPostsAsync(){
            try
            {
                await SyncAsync();
                return new List<Post>(await postTable.ReadAsync());
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"INVALID {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"ERROR {0}", e.Message);
            }
            return null;
        }

		//done
		public async Task savePost(Post item){



            try
            {
                if (item.Id == null)
                {
                    await postTable.InsertAsync(item);
                }
                else
                {
                    await postTable.UpdateAsync(item);
                }
                await SyncAsync();

                //Items.Add(todoItem);

            }
            catch (MobileServiceInvalidOperationException e)
            {
                Debug.WriteLine(@"ERROR {0}", e.Message);
            }

        }


        //done
		public async Task saveComment(Comment item){

            if (item.Id == null)
                await commentTable.InsertAsync(item);
            else
                await commentTable.UpdateAsync(item);
            await SyncAsync();
        }

        //fixing
        public async Task<List<CommentResult>> GetRelatedComment(string PostIdkey)
        {

            var commentResult = new List<CommentResult>();
            var comments = new List<Comment>();

            var post = await postTable.LookupAsync(PostIdkey);
           // var items = database.GetWithChildren<Post>(PostIdkey);



            foreach (var item in post.Comments)
            {
                var result = new CommentResult();
                var user = await userTable.LookupAsync(item.UserId);
                result.profileImageUrl = user.profileImageUrl;
                result.FName = user.FName;
                result.comment = item.text;
                commentResult.Add(result);
            }
            return commentResult;
        }

        //done
        public async Task<List<PostResult>> GetAllPosts()
        {
            var results = new List<PostResult>();

            await SyncAsync();
            var items = new List<Post>(await postTable.ReadAsync());


            foreach (var item in items)
            {
                var result = new PostResult();
                result.PostId = item.Id;
                result.profileImageUrl = item.User.profileImageUrl;
                result.PostDetail = item.PostDetail;
                result.PostTitle = item.PostTitle;
                result.Likes = item.Likes.ToString();
                results.Add(result);
            }
            return results;
        }

        public async Task SyncAsync()
        {
            try
            {
                await this.client.SyncContext.PushAsync();
                await this.userTable.PullAsync(null, userTable.CreateQuery());
                await this.postTable.PullAsync(null, postTable.CreateQuery());
                await this.commentTable.PullAsync(null, commentTable.CreateQuery());
            }
            catch (MobileServiceInvalidOperationException e)
            {
                Debug.WriteLine(@"Sync Failed: {0}", e.Message);
            }
        }



    }
}

