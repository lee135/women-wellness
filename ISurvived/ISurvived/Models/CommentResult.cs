using System;
using SQLite.Net;

using Xamarin.Forms;
using System.Linq;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Ioc;
using SQLiteNetExtensions.Extensions;
using System.Diagnostics;

namespace ISurvived
{
	public class CommentResult
	{
		public DateTime TimeStamp { get; set; }
		public string comment { get; set; }

		public string profileImageUrl { get; set; }
		public string FName { get; set; }

	}

}

