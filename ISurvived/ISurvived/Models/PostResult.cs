using System;
using SQLite.Net;

using Xamarin.Forms;
using System.Linq;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Ioc;
using SQLiteNetExtensions.Extensions;
using System.Diagnostics;

namespace ISurvived

{
	public class PostResult
	{
		public string PostId { get; set; }
		public string PostTitle { get; set; }
		public DateTime TimeStamp { get; set; }
		public string PostDetail { get; set; }
		public string Likes { get; set; }
		public string commentNumber { get; set; }
		public string profileImageUrl { get; set; }
		public string Fname { get; set; }
		public string Lname { get; set; }
	}

}

