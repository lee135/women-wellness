﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Collections.Generic;

namespace ISurvived

{
	public class MyMap:Map
	{
//		public List<Pin> CustomPins= new List<Pin>();

		public MyMap()
		//Default start position
			: base(MapSpan.FromCenterAndRadius(new Position(50.13072, 8.57092), Distance.FromKilometers(15.0)))
		{

		}

	}
}

