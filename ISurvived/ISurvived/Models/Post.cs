﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ISurvived

{
    [JsonObject]
    public class Post
    {
        public string Id { get; set; }

        [JsonProperty(PropertyName = "PostTitle")]
        public string PostTitle { get; set; }

        [JsonProperty(PropertyName = "PostDetail")]
        public string PostDetail { get; set; }

        [JsonProperty(PropertyName = "Likes")]
        public int Likes { get; set; }

        [JsonProperty(PropertyName = "imageURL")]
        public string imageURL { get; set; }

        [JsonProperty(PropertyName = "commentNumber")]
        public int commentNumber { get; set; }

        [JsonProperty(PropertyName = "emotion")]
        public string emotion { get; set; }

        [JsonProperty(PropertyName = "longitude")]
        public long longitude { get; set; }

        [JsonProperty(PropertyName = "latitude")]
        public long latitude { get; set; }

        [JsonProperty(PropertyName = "UserId")]
        public string UserId { get; set; }

        [JsonProperty("User")]
        public User User
        {
            get;
            set;
        }

        [JsonProperty("Comments")]
        public ICollection<Comment> Comments { get; set; }

        public Post()
        {
            Comments = new List<Comment>();
        }

    }
}
