﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
namespace ISurvived
{
     [JsonObject]
    public class Comment
	{

        public string Id { get; set; }

        [JsonProperty("comment")]
        public string text { get; set; }


        [JsonProperty("UserId")]
        public string UserId { get; set; }

        [JsonProperty("User")]
        public User User
        {
            get;
            set;
        }

        [JsonProperty("PostId")]
        public string PostId { get; set; }

        [JsonProperty("Post")]
        public Post Post
        {
            get;
            set;
        }
    }
}

