﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
namespace ISurvived
{
    [JsonObject]
    public class User
	{
        public string Id { get; set; }


        [JsonProperty(PropertyName = "Fname")]
        public string FName { get; set; }

        [JsonProperty(PropertyName = "LName")]
        public string LName { get; set; }

        [JsonProperty(PropertyName = "hashedPassword")]
        public string hashedPassword { get; set; }

        [JsonProperty(PropertyName = "profileImageUrl")]
        public string profileImageUrl { get; set; }

        [JsonProperty(PropertyName = "longitude")]
        public long longitude { get; set; }

        [JsonProperty(PropertyName = "latitude")]
        public long latitude { get; set; }

        [JsonProperty("Posts")]
        public ICollection<Post> Posts { get; set; }

        [JsonProperty("Comments")]
        public ICollection<Comment> Comments { get; set; }


    }
}
