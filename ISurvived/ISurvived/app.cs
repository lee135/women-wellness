﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ISurvived
{

    public class App : Application
	{



		private static ViewModelLocator _locator;

        #region Azure stuff
        static ForumService forumService;

        public static ForumService ForumService
        {
            get { return forumService; }
            set { forumService = value; }
        }

        public static void SetForumService(ForumService forumService)
        {
            ForumService = forumService;
        }
        #endregion

        public static ViewModelLocator Locator
		{
			get
			{
				return _locator ?? (_locator = new ViewModelLocator());
			}
		}

	

		public App ()
		{

           
			// The root page of your application
			MainPage = new TabViews();

           
    }




    protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

        public class PostExpandHandler : DelegatingHandler
        {
            protected override async Task<HttpResponseMessage>
            SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                bool requestToTodoTable = request.RequestUri.PathAndQuery
                    .StartsWith("/tables/post", StringComparison.OrdinalIgnoreCase)
                        && request.Method == HttpMethod.Get;
                if (requestToTodoTable)
                {
                    UriBuilder builder = new UriBuilder(request.RequestUri);
                    string query = builder.Query;
                    if (!query.Contains("$expand"))
                    {
                        if (string.IsNullOrEmpty(query))
                        {
                            query = string.Empty;
                        }
                        else
                        {
                            query = query + "&";
                        }

                        query = query + "$expand=comments,user";
                        builder.Query = query.TrimStart('?');
                        request.RequestUri = builder.Uri;
                    }
                }

                var result = await base.SendAsync(request, cancellationToken);
                return result;
            }
        }





    }


}

