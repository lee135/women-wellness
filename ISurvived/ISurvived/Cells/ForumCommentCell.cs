﻿using System;

using Xamarin.Forms;

namespace ISurvived
{
	public class ForumCommentCell : ViewCell
	{
		public ForumCommentCell ()
		{
			Height = 100;

			var image = new Image
			{
				HeightRequest = 50,
				WidthRequest = 50,
				HorizontalOptions = LayoutOptions.Start,
			};
			image.SetBinding(Image.SourceProperty, new Binding("profileImageUrl"));

			var NameLabel = new Label
			{
				FontAttributes=FontAttributes.None,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				VerticalOptions = LayoutOptions.Start,
				HorizontalOptions= LayoutOptions.StartAndExpand
			};
			NameLabel.SetBinding(Label.TextProperty, "FName");

			var CommentLabel = new Label
			{
				FontSize = 12,
				TextColor = Color.FromHex ("#666"),
				VerticalOptions = LayoutOptions.Start,
				HorizontalOptions = LayoutOptions.StartAndExpand
			};
			CommentLabel.SetBinding(Label.TextProperty, "comment");


			var commentLayout = new StackLayout {
				Orientation = StackOrientation.Vertical,
				VerticalOptions = LayoutOptions.Start,
				Children = { NameLabel, CommentLabel }

			};
					
	
			View = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				VerticalOptions = LayoutOptions.Start,
				Children = {image, commentLayout }
			};
		}
	}
}


