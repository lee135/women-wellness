﻿using Xamarin.Forms;
using System;


class ForumPostCell : ViewCell
{
	public ForumPostCell()
	{
		var image = new Image
		{
//			BorderColor = App.BrandColor,
//			BorderThickness = 2,
			HeightRequest = 50,
			WidthRequest = 50,
//			Aspect = Aspect.AspectFill,

			HorizontalOptions = LayoutOptions.End,
//			VerticalOptions = LayoutOptions.Center,
		};
		image.SetBinding(Image.SourceProperty, new Binding("profileImageUrl"));
		var commentNumber = CreateRelativeLayout ();
		var nameLayout = CreateNameLayout();

		var viewLayout = new StackLayout()
		{
			Orientation = StackOrientation.Horizontal,
			HorizontalOptions=LayoutOptions.Start,
			Padding = new Thickness (10, 10, 10, 10),
//			Spacing = 10,
			Children = { commentNumber,nameLayout,image }
		};
		View = viewLayout;
	}



	static StackLayout CreateNameLayout()
	{

		var titleLabel = new Label
		{
			FontAttributes=FontAttributes.None,
			FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
			VerticalOptions = LayoutOptions.CenterAndExpand,
			HorizontalOptions= LayoutOptions.StartAndExpand
		};
		titleLabel.SetBinding(Label.TextProperty, "PostTitle");

		var detailLabel = new Label
		{
			FontSize = 12,
			TextColor = Color.FromHex ("#666"),
			VerticalOptions = LayoutOptions.CenterAndExpand,
			HorizontalOptions = LayoutOptions.StartAndExpand
		};
		detailLabel.SetBinding(Label.TextProperty, "PostDetail");


		var nameLayout = new StackLayout()
		{
			HorizontalOptions = LayoutOptions.StartAndExpand,
			Orientation = StackOrientation.Vertical,
			Children = { titleLabel, detailLabel }
		};
		return nameLayout;
	}

	public static RelativeLayout CreateRelativeLayout ()
	{   

		var myLabel = new Label
		{
			FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
			XAlign = TextAlignment.Start,
			YAlign = TextAlignment.Start
		};

		myLabel.SetBinding(Label.TextProperty, "commentNumber");


		var myImage = new Image () {
			Source = "tap.png",
		};

		RelativeLayout layout = new RelativeLayout ();

		layout.Children.Add (myImage, 
			Constraint.RelativeToParent ((parent) => {
				return parent.Width/13 ;
			}),
			Constraint.RelativeToParent ((parent) => {
				return parent.Height/20;
			})
		);

		layout.Children.Add (myLabel, 
			Constraint.RelativeToParent ((parent) => {
				return myImage.Width/2.2;
			}),
			Constraint.RelativeToParent ((parent) => {
				return myImage.Height/2;
			})
		);

		layout.HorizontalOptions = LayoutOptions.Start;

		return layout;
	}
}