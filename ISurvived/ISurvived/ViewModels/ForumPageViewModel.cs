﻿using GalaSoft.MvvmLight;
using System.Collections.Generic;
using System;
using System.Windows.Input;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using ISurvived;
using System.Threading.Tasks;

namespace ISurvived
{
	public class ForumPageViewModel:ViewModelBase
	{
		private IMyNavigationService navigationService;
		private ObservableCollection<PostResult> postList{ get; set;}
		public ObservableCollection<PostResult> PostList {
			get {return postList;}
			set{ postList = value; 
				RaisePropertyChanged (() => PostList);
			}
		}

		ObservableCollection<CommentResult> contents = new ObservableCollection<CommentResult> ();
		public ObservableCollection<CommentResult> Contents { 
			get { return contents; } 
			set
			{
				if (contents == value)
					return;
				contents = value;
				RaisePropertyChanged ();
			}
		}


		public ICommand NewPostCommand { get; private set; }

		public ICommand PostListCommand {get; private set;}

		public ICommand ClearSearchCommand { get; private set; }

		private string searchTerm { get; set; }

		public string SearchTerm{
			get { return searchTerm; }
			set { 
				if (value!= null) {
					searchTerm = value;
					RaisePropertyChanged (() => SearchTerm);
				}
			}
		}
		public ForumPageViewModel (IMyNavigationService navigationService)
		{
            this.navigationService = navigationService;

			//only run init once in showcase, commentout it if it's not init-running
			//database.initForumdatabase ();

		


			NewPostCommand = new Command (() => this.navigationService.NavigateTo (ViewModelLocator.NewPostPageKey));
//			PostListCommand = new Command (() => {
//				PostList = new ObservableCollection<PostResult>(database.SearchTitleDetail(SearchTerm));
//			});
//			ClearSearchCommand = new Command (() => {
//				PostList = new ObservableCollection<Post> (database.GetAllPosts ());
//				SearchTerm = string.Empty;
//			});
		}


        public async Task OnAppearing()
        {
            PostList = new ObservableCollection<PostResult>(await App.ForumService.GetAllPosts());
        }

        object selectedItem;
		public object SelectedItem
		{
			get { return selectedItem; }
			set
			{
				if (selectedItem == value)
					return;
				// something was selected
				selectedItem = value;

				RaisePropertyChanged ();

				if (selectedItem != null) {
					navigationService.NavigateTo ("ForumDetailPage",selectedItem);

					selectedItem = null;
				}
			}
		}


	}
}

