﻿using System;
using GalaSoft.MvvmLight;
using System.Windows.Input;
using Xamarin.Forms;
using Microsoft.Practices.ServiceLocation;
using System.Collections.Generic;

namespace ISurvived
{
	public class NewPostViewModel:ViewModelBase
	{

		private IMyNavigationService navigationService;
		public ICommand SavePostCommand { get; private set;}


		private String postTitle;

		public String PostTitle
		{
			get { return postTitle; }
			set { postTitle = value;
				RaisePropertyChanged(() => PostTitle); }
		}

		private string postDetail;

		public string PostDetail
		{
			get { return postDetail; }
			set { postDetail = value;
				RaisePropertyChanged (() => PostDetail); }
		}

		public NewPostViewModel (IMyNavigationService navigationService)
		{
			this.navigationService = navigationService;

			SavePostCommand = new Command (async () =>
            {

                await App.ForumService.savePost(
                     new Post()
                     {
                         UserId = "2",
                         PostDetail = this.PostDetail,
                         PostTitle = this.PostTitle
                     }
                );
                navigationService.GoBack();

            });
		}
	}
}

