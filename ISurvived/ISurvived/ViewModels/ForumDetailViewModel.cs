﻿using System;
using GalaSoft.MvvmLight;
using System.Windows.Input;
using Xamarin.Forms;
using Microsoft.Practices.ServiceLocation;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ISurvived
{
	public class ForumDetailViewModel:ViewModelBase
	{

		private IMyNavigationService navigationService;

		public ICommand SaveCommentCommand { get; private set;}


		private ObservableCollection<CommentResult> commentList;

		public ObservableCollection<CommentResult> CommentList
		{
			get { return commentList; }
			set { commentList = value;
				RaisePropertyChanged (() => CommentList); }
		}

		private String newComment;

		public String NewComment
		{
			get { return newComment; }
			set { newComment = value;
				RaisePropertyChanged (() => NewComment); }
		}



		public ForumDetailViewModel (IMyNavigationService navigationService)
		{
			this.navigationService = navigationService;
		}

        public async Task OnAppearing(PostResult itemSelected)
        {

            //			CurrentPlatform.Init();
            //			Item item = new Item { Text = "Awesome item" };
            //			await AppDelegate.MobileService.GetTable<Item>().InsertAsync(item);
            CommentList = new ObservableCollection<CommentResult>(await App.ForumService.GetRelatedComment(itemSelected.PostId));
        }


        public async void Reload(PostResult itemSelected)
        {

            CommentList = new ObservableCollection<CommentResult>
                (await App.ForumService.GetRelatedComment(itemSelected.PostId));


        }





        public async Task SaveComment(PostResult result)
        {
            await App.ForumService.saveComment(new Comment()
            {
                text = NewComment,
                UserId = "2",
                PostId = result.PostId

            });



        }

    }
}

