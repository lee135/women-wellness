﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Diagnostics;

namespace ISurvived
{
	public class MapPage : ContentPage {
		public MapPage() {
			var map = new MyMap(){
				IsShowingUser = true,
				HeightRequest = 100,
				WidthRequest = 960,
				VerticalOptions = LayoutOptions.FillAndExpand,
				MapType = MapType.Street
			};
			var stack = new StackLayout { Spacing = 0 };
			stack.Children.Add(map);
			Content = stack;

			//Create a Pin
			Pin pin = new Pin()
			{
				Position = new Position(50.13072, 8.57092),
				Label = "Zühlke Engineering GmbH",
				Address = "Düsseldorfer Straße 40a, 65760 Eschborn"
			};

			//Workaround for iOS
			MessagingCenter.Subscribe<Pin>(this, "iOSPinSelected", (sender) =>
				{
					pin_Clicked(sender, null);
				});

			//This only works on Android
			pin.Clicked += pin_Clicked;

			//Add the Pin to the map.
			map.Pins.Add(pin);

		}
		void pin_Clicked(object sender, EventArgs e)
		{
			Debug.WriteLine("Pin clicked:" + ((Pin)sender).Address);
		}

	}
}
