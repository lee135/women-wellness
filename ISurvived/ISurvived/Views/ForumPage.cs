﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Practices.ServiceLocation;

namespace ISurvived
{
	public class ForumPage : ContentPage
	{


		public ForumPage ()
		{	

			BindingContext = App.Locator.ForumPageVM;

			if (Device.OS == TargetPlatform.Android) {
				NavigationPage.SetHasNavigationBar (this, false);
			}

		

			var header = new Image 
			{	
				Source="forum_banner2.png",
				//				Aspect = Aspect.AspectFit,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Start,
			};

			var listView = new ListView
			{	
				IsPullToRefreshEnabled = true,
				RowHeight = 60,
				SeparatorColor = Color.Silver,
			};


			listView.Refreshing += OnRefresh;
			//			listView.ItemSelected += OnSelection;
//			listView.ItemTapped += OnTap;


			listView.ItemTemplate = new DataTemplate(typeof(ForumPostCell));
			listView.ItemsSource = App.Locator.ForumPageVM.PostList;
			listView.SetBinding (ListView.ItemsSourceProperty, "PostList");
			listView.SetBinding (ListView.SelectedItemProperty, new Binding ("SelectedItem", BindingMode.TwoWay));


			var createPostButton = new Button {

				Text ="Add new Post",
				HeightRequest=40,
				WidthRequest=100

			};
			createPostButton.SetBinding (Button.CommandProperty,"NewPostCommand");

			RelativeLayout layout = new RelativeLayout ();

			layout.Children.Add (header, Constraint.RelativeToParent ((parent) => {
				return 0;
			}));

			layout.Children.Add (createPostButton,
				Constraint.RelativeToParent ((parent) => {
					return header.Width / 1.5;
				}),
				Constraint.RelativeToParent ((parent) => {
					return header.Height / 1.5;
				}));


			layout.HorizontalOptions = LayoutOptions.Start;

			Content = new StackLayout
			{
				Spacing=10,

				VerticalOptions = LayoutOptions.Start,
				Children = {layout,listView }
			};


//			// Accomodate iPhone status bar.
//			this.Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);


		}



        //		void OnTap (object sender, ItemTappedEventArgs e)
        //		{
        //
        //			Navigation.PushAsync(new ForumDetailPage());
        //		}

        //		void OnSelection (object sender, SelectedItemChangedEventArgs e)
        //		{
        //			if (e.SelectedItem == null) {
        //				return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
        //			}
        //			DisplayAlert ("Item Selected", e.SelectedItem.ToString (), "Ok");
        //			//comment out if you want to keep selections
        //			ListView lst = (ListView)sender;
        //			lst.SelectedItem = null;
        //		}

        async void OnRefresh(object sender, EventArgs e)
        {
            var list = (ListView)sender;

            //put your refreshing logic here


            await App.ForumService.SyncAsync();
            var vm = ServiceLocator.Current.GetInstance<ForumPageViewModel>();
            await vm.OnAppearing();

            //make sure to end the refresh state
            list.IsRefreshing = false;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var vm = ServiceLocator.Current.GetInstance<ForumPageViewModel>();
            await vm.OnAppearing();
            // Item item = new Item() { Text = "Awesome item" };
            //await App.TodoManager.SaveTaskAsync(item);

        }


    }


		

	}



