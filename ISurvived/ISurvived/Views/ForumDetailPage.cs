﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;

namespace ISurvived
{
	public class ForumDetailPage : ContentPage
	{
		PostResult ItemSelected;


		public ForumDetailPage (PostResult itemSelected )
		{
			this.ItemSelected = itemSelected;

			if (Device.OS == TargetPlatform.Android) {
				NavigationPage.SetHasNavigationBar (this, false);
			}

			BindingContext = App.Locator.ForumDetailPageVM;

			//Header
			var header = new Image 
			{	
				Source="forum_banner2.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Start,
			};



			//GridView
			var gridView = new Grid {VerticalOptions = LayoutOptions.StartAndExpand, HeightRequest=160};
			gridView.ColumnDefinitions.Add(new ColumnDefinition() {Width = new GridLength(1,GridUnitType.Auto)});
			gridView.ColumnDefinitions.Add(new ColumnDefinition() {Width = new GridLength(3,GridUnitType.Auto)});
			gridView.RowDefinitions.Add(new RowDefinition( ){Height = new GridLength(1.5,GridUnitType.Auto)});
			gridView.RowDefinitions.Add(new RowDefinition(){ Height = new GridLength(1,GridUnitType.Auto)});
			var image = new Image {

				HeightRequest=40,
				WidthRequest=40,

			};
			image.Source = ImageSource.FromFile(this.ItemSelected.profileImageUrl);


			Grid.SetRowSpan (image, 2);
			Grid.SetColumn (image, 0);
			gridView.Children.Add (image);

			var firstRow = new Label (){ Text = this.ItemSelected.PostTitle };
			Grid.SetRow (firstRow, 0);
			Grid.SetColumn (firstRow, 1);
			gridView.Children.Add (firstRow);

			var subTitleRow = new Label (){ Text = this.ItemSelected.PostDetail  };
			Grid.SetRow (subTitleRow, 1);
			Grid.SetColumn (subTitleRow, 1);
			gridView.Children.Add (subTitleRow);

			var secondRow = new Label (){ Text = "4 Comments", VerticalOptions =LayoutOptions.Center };

			Grid.SetRow (secondRow, 2);
			Grid.SetColumnSpan (secondRow, 2);
			gridView.Children.Add (secondRow);


            //List View
            var listView = new ListView
            {
                IsPullToRefreshEnabled = true,
				SeparatorColor = Color.Silver,
				VerticalOptions = LayoutOptions.End,
			};
			listView.ItemsSource = App.Locator.ForumDetailPageVM.CommentList;
			listView.SetBinding(ListView.ItemsSourceProperty, "CommentList");
			listView.ItemTemplate = new DataTemplate(typeof(ForumCommentCell));
            listView.Refreshing += OnRefresh;

            //CommentStack
            var commentStack = new StackLayout (){Orientation = StackOrientation.Horizontal };
			var commentLabel = new Label (){ Text = "Comment:", VerticalOptions = LayoutOptions.CenterAndExpand };
			var commentEntry = new Entry (){Placeholder ="What's on your mind?" , HorizontalOptions =LayoutOptions.EndAndExpand,WidthRequest = 210};
			commentEntry.SetBinding (Entry.TextProperty, "NewComment");
			commentEntry.Completed += async (s, e) =>
            {
                var vm = ServiceLocator.Current.GetInstance<ForumDetailViewModel>();

                await vm.SaveComment(ItemSelected);
                vm.Reload(ItemSelected);
                commentEntry.Text = "";

            };


			commentStack.Children.Add (commentLabel);
			commentStack.Children.Add (commentEntry);

			var mainStack = new StackLayout { 
				Orientation = StackOrientation.Vertical,
				VerticalOptions = LayoutOptions.Start
			};


			Content = mainStack;
			mainStack.Children.Add (header);
			mainStack.Children.Add (gridView);
			mainStack.Children.Add (listView);
			mainStack.Children.Add (commentStack);

		}

        async void OnRefresh(object sender, EventArgs e)
        {
            var list = (ListView)sender;

            //put your refreshing logic here


            await App.ForumService.SyncAsync();
            var vm = ServiceLocator.Current.GetInstance<ForumDetailViewModel>();
            await vm.OnAppearing(this.ItemSelected);

            //make sure to end the refresh state
            list.IsRefreshing = false;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var vm = ServiceLocator.Current.GetInstance<ForumDetailViewModel>();
            await vm.OnAppearing(this.ItemSelected);
        }





    }
}


