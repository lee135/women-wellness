﻿
using Xamarin.Forms;

namespace ISurvived
{
    public partial class NewPostPage : ContentPage
	{
		public NewPostPage ()
		{
			if (Device.OS == TargetPlatform.Android) {
				NavigationPage.SetHasNavigationBar (this, false);
			}

			BindingContext = App.Locator.NewPostVM;

			InitializeComponent ();
		}
	}
}

