﻿using System;
using GalaSoft.MvvmLight.Ioc;
using Xamarin.Forms;

namespace ISurvived
{
	class TabViews: TabbedPage
	{	

		private static NavigationService nav;
			public TabViews ()
			{


			this.Children.Add (ForumNaviPage());



			this.Children.Add (new OtherPage {
				Title = "User",

			}
			);






			this.Children.Add (new MapPage {
				Title = "Map",

			});
				


		}
		public Page ForumNaviPage()
		{
			nav = new NavigationService ();
			nav.Configure (ViewModelLocator.ForumPageKey, typeof(ForumPage));
			nav.Configure (ViewModelLocator.ForumDetailPageKey, typeof(ForumDetailPage));
			nav.Configure (ViewModelLocator.NewPostPageKey, typeof(NewPostPage));
			SimpleIoc.Default.Register<IMyNavigationService> (()=> nav, true);
			var navPage = new NavigationPage (new ForumPage ());
			navPage.Title ="Forum";
			nav.Initialize (navPage);
			return navPage;
		}
}
}