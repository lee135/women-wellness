﻿using System;
using SQLite.Net;

namespace ISurvived
{
	public interface ISqlite
	{
		SQLiteConnection GetConnection();
	}
}

