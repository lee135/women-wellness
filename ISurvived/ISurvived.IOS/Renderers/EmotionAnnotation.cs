﻿using System;
using CoreLocation;
using MapKit;

namespace ISurvived.IOS
{
	public class EmotionAnnotation : MKAnnotation
	{
		string title;
		CLLocationCoordinate2D coord;

		public EmotionAnnotation (string title, CLLocationCoordinate2D coord)
		{
			this.title = title;
			this.coord = coord;
		}

		public override string Title {
			get {
				return title;
			}
		}

		public override CLLocationCoordinate2D Coordinate {
			get {
				return coord;
			}
		}
	}
}


