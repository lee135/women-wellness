﻿using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms.Maps;
using Xamarin.Forms;
using ISurvived.IOS;
using MapKit;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using ISurvived;

[assembly: ExportRenderer(typeof(MyMap), typeof(MyMapRenderer))]
namespace ISurvived.IOS
{
    public class MyMapRenderer : MapRenderer
	{


		public class MyMapViewDelegate : MKMapViewDelegate
		{
			private Map Map { get; set; }

			public MyMapViewDelegate(Map map)
			{

				Map = map;
			}



			public override MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
			{
				//The users location is handled by the os
				if (mapView.UserLocation.Location != null && annotation.Coordinate.Equals(mapView.UserLocation.Location.Coordinate))
				{
					return null;
				}

				string mId = "EmotionAnnotation";
				//const string defaultPinIdentifier = "defaultPin";
				MKAnnotationView anView;
				anView = mapView.DequeueReusableAnnotation (mId);
//				MKPinAnnotationView pinAnnotationView = (MKPinAnnotationView)mapView.DequeueReusableAnnotation(defaultPinIdentifier);

				//This is where we can customize the pin (callout, image, ...)
				if (anView == null)
				{

					anView = new MKAnnotationView (annotation, mId);

					anView.Image = UIImage.FromFile ("happy.png");
					anView.CanShowCallout = true;
					anView.Draggable = true;
					anView.RightCalloutAccessoryView = UIButton.FromType (UIButtonType.DetailDisclosure);
//					pinAnnotationView = new MKPinAnnotationView(annotation, defaultPinIdentifier)
//					{
//						CanShowCallout = true,
//						Annotation = annotation, 
//						Image = UIImage.FromFile ("happy.png")
//					};
//
//					UIButton rightButton = new UIButton(UIButtonType.DetailDisclosure);
//					pinAnnotationView.RightCalloutAccessoryView = rightButton;
				}
				else
				{
					anView.Annotation = annotation;
				}
				return anView; 
			}

			public override void CalloutAccessoryControlTapped(MKMapView mapView, MKAnnotationView view, UIControl control)
			{
				foreach (Pin pin in Map)
				{
					if (view.Annotation.Coordinate.Latitude.Equals(pin.Position.Latitude) &&
						view.Annotation.Coordinate.Longitude.Equals(pin.Position.Longitude))
					{
						MessagingCenter.Send(pin, "iOSPinSelected");
					}
				}
			}
		}

		private MKMapView Map { get { return Control as MKMapView; } }



		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged(e);
			if (Map != null)
			{
				Map.Delegate = null;
				//Entry point into the iOS world
				Map.Delegate = new MyMapViewDelegate((MyMap)e.NewElement);
			}
		}
	}

}

