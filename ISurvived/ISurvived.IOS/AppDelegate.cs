﻿using Foundation;
using Microsoft.WindowsAzure.MobileServices;
using UIKit;
using Microsoft.WindowsAzure.MobileServices.Sync;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using System.Threading.Tasks;

namespace ISurvived
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
    [Register ("AppDelegate")]
	public partial class AppDelegate : 
	global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate // superclass new in 1.3
	{
        MobileServiceClient Client;
        ForumService ForumService;

        public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{



            Xamarin.FormsMaps.Init();
			global::Xamarin.Forms.Forms.Init ();

            #region Azure stuff
            CurrentPlatform.Init();
            SQLitePCL.CurrentPlatform.Init();
            Client = new MobileServiceClient(
                Constants.Url,
                Constants.Key, new App.PostExpandHandler());


            #region Azure Sync stuff
            // http://azure.microsoft.com/en-us/documentation/articles/mobile-services-xamarin-android-get-started-offline-data/
            // new code to initialize the SQLite store
            InitializeStoreAsync().Wait();
            #endregion


            ForumService = new ForumService(Client);

            App.SetForumService(ForumService);
            #endregion region


            LoadApplication(new ISurvived.App ());  // method is new in 1.3

			return base.FinishedLaunching (app, options);

		}

        public async Task InitializeStoreAsync()
        {
            string path = "syncstore.db";
            var store = new MobileServiceSQLiteStore(path);
            store.DefineTable<User>();
            store.DefineTable<Post>();
            store.DefineTable<Comment>();
            await Client.SyncContext.InitializeAsync(store, new MobileServiceSyncHandler());
            //				await Client.SyncContext.InitializeAsync (store);
        }

    }
}


