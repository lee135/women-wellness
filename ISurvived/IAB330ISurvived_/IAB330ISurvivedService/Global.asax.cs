﻿using System.Web.Http;
using System.Web.Routing;

namespace IAB330ISurvivedService
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            WebApiConfig.Register();
        }
    }
}