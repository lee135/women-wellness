﻿using Microsoft.WindowsAzure.Mobile.Service;
using System.Collections.Generic;

namespace ISurvivedService.DataObjects
{
    public class Comment : EntityData
    {


        public string comment { get; set; }

        public string imageURL { get; set; }

        public string UserId { get; set; }
        // Many to one relationship with User
        public virtual User User { get; set; }

        // Many to one relationship with User
        public string PostId { get; set; }
        public virtual Post Post { get; set; }
    }
}
