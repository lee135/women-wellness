﻿using Microsoft.WindowsAzure.Mobile.Service;

using System.Collections.Generic;

namespace ISurvivedService.DataObjects
{
    public class Post : EntityData
    {

        public Post()
        {
            Comments = new List<Comment>();
        }
       
        public string PostTitle { get; set; }
        public string PostDetail { get; set; }  
        
        public string imageURL { get; set; }
        public int Likes { get; set; }
        public int commentNumber { get; set; }
        public string emotion { get; set; }
        public long longitude { get; set; }
        public long latitude { get; set; }

        // Many to one relationship with User
        public string UserId { get; set; }
        public virtual User User { get; set; }

       // One to many relationship with Comment
        public virtual ICollection<Comment> Comments { get; set; }

    }
}