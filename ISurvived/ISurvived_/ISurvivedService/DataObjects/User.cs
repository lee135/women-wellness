﻿using Microsoft.WindowsAzure.Mobile.Service;
using System.Collections.Generic;

namespace ISurvivedService.DataObjects
{
    public class User : EntityData
    {

        public User()
        {
            Comments = new List<Comment>();
            Posts = new List<Post>();
        }

        public string FName { get; set; }
        public string LName { get; set; }
        public string hashedPassword { get; set; }
        public string profileImageUrl { get; set; }   
        public long longitude { get; set; }
        public long latitude { get; set; }

        // One to many relationship with Post
        public virtual ICollection<Post> Posts { get; set; }

        // One to many relationship with Comments
        public virtual ICollection<Comment> Comments { get; set; }
    }
}