﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service;
using ISurvivedService.DataObjects;
using ISurvivedService.Models;

namespace ISurvivedService
{
    public static class WebApiConfig
    {
        public static void Register()
        {
            // Use this class to set configuration options for your mobile service
            ConfigOptions options = new ConfigOptions();

            // Use this class to set WebAPI configuration options
            HttpConfiguration config = ServiceConfig.Initialize(new ConfigBuilder(options));

            // To display errors in the browser during development, uncomment the following
            // line. Comment it out again when you deploy your service for production use.
            // config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
            
            // Set default and null value handling to "Include" for Json Serializer
            config.Formatters.JsonFormatter.SerializerSettings.DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Include;
            config.Formatters.JsonFormatter.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Include;
            
            Database.SetInitializer(new ISurvivedInitializer());
        }
    }

    public class ISurvivedInitializer : ClearDatabaseSchemaIfModelChanges<ISurvivedContext>
    {
        protected override void Seed(ISurvivedContext context)
        {

            List<Comment> comments = new List<Comment>
            {
               new Comment {Id="1",comment = "yes of course you can do it"},
               new Comment {Id="2",comment = "Gongbadie"},
               new Comment {Id="3",comment = "I agree with Jane"},
               new Comment {Id="4",comment = "Go girl!"},
               new Comment {Id="5",comment = "давай！"}
            };

           

            List<Post> posts = new List<Post>
            {
               new Post {Id="1",PostTitle = "My experience",PostDetail = "Well, I thought some one would...",Comments =comments},
               new Post {Id="2",PostTitle = "With or Without you",PostDetail ="Hey guys, I was just wondering"},
               new Post {Id="3",PostTitle = "Tip this much in",PostDetail ="Tip like a local all over the world"},
               new Post {Id="4",PostTitle = "Looking for new friends", PostDetail = "Is there anybody here interested"},
               new Post {Id="5",PostTitle = "Advice Please", PostDetail ="Hello I need an advice and i think", }
            };


            List<User> users = new List<User>
            {
               new User {Id="1",FName = "Jane",profileImageUrl ="image1.png",Comments ={ comments[0] },Posts = { posts[0] } },
               new User {Id="2",FName = "Sara",profileImageUrl="image2.png",Comments ={ comments[1] },Posts = { posts[1] }},
               new User {Id="3",FName ="Iris", profileImageUrl ="image3.png",Comments ={ comments[2] },Posts = { posts[2] }},
               new User {Id="4",FName = "Alice",profileImageUrl ="image4.png",Comments ={ comments[3] },Posts = { posts[3] }},
               new User {Id="5",FName ="Carlie",profileImageUrl ="image5.png",Comments ={ comments[4] },Posts = { posts[4] } }
            };



            foreach (Comment comment in comments)
            {
                context.Set<Comment>().Add(comment);
            }

            foreach (User user in users)
            {
                context.Set<User>().Add(user);
            }

            foreach (Post post in posts)
            {
                context.Set<Post>().Add(post); 
            }

            base.Seed(context);


           
            //foreach (Comment comment in comments)
            // {
            //     context.Comments.Add(comment);
            // }
            
            // base.Seed(context);
        }
    }
}

